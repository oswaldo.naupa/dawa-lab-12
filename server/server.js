const express = require('express');

const socketIO = require("socket.io");

const http = require("http");

const path = require('path');

const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

//IO = esta en la comunicacion del backend
let io = socketIO.listen(server);

io.on("connection", (cliente) => {
    console.log("Usuario conectado");

    cliente.emit('enviarMensaje', {
        usuario: "Administrador",
        mensaje: 'Bienvenido a esta aplicacion'
    })

    cliente.on("disconnect", ()=>{
        console.log("Usuario desconectado");
    });

    cliente.on("enviarMensaje", (data, callback)=>{
        console.log(data);

        cliente.broadcast.emit("enviarMensaje",data);
    });

    //Escuchar el cliente

    cliente.on("enviarMensaje",(mensaje) => {
        console.log(mensaje);
    });
});




server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});